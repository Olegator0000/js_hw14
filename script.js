const lighttheme = document.querySelector("#lightTheme");
const darkTheme = document.querySelector("#darkTheme");
const button = document.querySelector(".toggle");
const lightHref = lighttheme.getAttribute("href");
const darkHref = darkTheme.getAttribute("href");
const currentTheme = localStorage.getItem("theme");

if (currentTheme === null) {
  localStorage.setItem("theme", `${lightHref}`);
}

button.addEventListener("click", function () {
  if (lighttheme.getAttribute("disabled") === null) {
    lighttheme.setAttribute("disabled", true);
    darkTheme.removeAttribute("disabled");
    localStorage.setItem("theme", `${darkHref}`);
  } else {
    darkTheme.setAttribute("disabled", true);
    lighttheme.removeAttribute("disabled");
    localStorage.setItem("theme", `${lightHref}`);
  }
});

if (currentTheme) {
  if (currentTheme === lightHref) {
    lighttheme.removeAttribute("disabled");
    darkTheme.setAttribute("disabled", true);
  } else if (currentTheme === darkHref) {
    darkTheme.removeAttribute("disabled");
    lighttheme.setAttribute("disabled", true);
  }
}
